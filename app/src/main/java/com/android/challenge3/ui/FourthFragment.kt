package com.android.challenge3.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.challenge3.data.Person
import com.android.challenge3.databinding.FragmentFourthBinding
import com.android.challenge3.utils.EXTRA_DATA_PERSON
import com.android.challenge3.utils.showToastShort


class FourthFragment : Fragment() {
    private var _binding: FragmentFourthBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentFourthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnMove.setOnClickListener {
                val name = FourthFragmentArgs.fromBundle(arguments as Bundle).name
                val age = edtUsia.text.toString()
                val address = edtAlamat.text.toString()
                val job = edtPekerjaan.text.toString()
                if (name?.isNotEmpty() == true && age.isNotEmpty() && address.isNotEmpty() && job.isNotEmpty()) {
                    val person = Person(name, age.toInt(), address, job)
                    findNavController().previousBackStackEntry?.savedStateHandle?.set(
                        EXTRA_DATA_PERSON,person)
                    findNavController().navigateUp()
                } else context?.showToastShort("Lengkapi Data")
            }
        }
    }

}