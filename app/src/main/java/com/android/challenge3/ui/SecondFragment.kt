package com.android.challenge3.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.challenge3.databinding.FragmentSecondBinding
import com.android.challenge3.utils.showToastShort


class SecondFragment : Fragment() {
    private var _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            btnMove.setOnClickListener {
                if (edtName.text.toString().isNotEmpty()){
                    val action = SecondFragmentDirections.actionSecondFragmentToThirdFragment()
                    action.name = edtName.text.toString()
                    findNavController().navigate(action)
                } else context?.showToastShort("Lengkapi Data")
            }
        }
    }

}