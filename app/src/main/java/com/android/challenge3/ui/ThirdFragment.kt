package com.android.challenge3.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.challenge3.R
import com.android.challenge3.data.Person
import com.android.challenge3.databinding.FragmentThirdBinding
import com.android.challenge3.utils.*


class ThirdFragment : Fragment() {
    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.apply {
            var name = ThirdFragmentArgs.fromBundle(arguments as Bundle).name ?: ""
            txtPersonInfo.text = name
            findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Person>(
                EXTRA_DATA_PERSON
            )?.observe(viewLifecycleOwner) { data ->
                data.apply {
                    name = nama.toString()
                    val status = checkGanjilGenap()
                    String.format(
                        getString(R.string.person_info),
                        nama, umur, status, alamat, pekerjaan
                    ).also { txtPersonInfo.text = it }
                }
            }
            btnMove.setOnClickListener {
                val action = ThirdFragmentDirections.actionThirdFragmentToFourthFragment()
                action.name = name
                findNavController().navigate(action)
            }
        }
    }

    private fun Person.checkGanjilGenap() = if ((umur?.rem(2) ?: 0) == 0) "Genap" else "Ganjil"

}