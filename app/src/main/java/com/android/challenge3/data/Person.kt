package com.android.challenge3.data

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Person(
    val nama: String?="",
    val umur: Int?=0,
    val alamat: String?="",
    val pekerjaan: String?=""
): Parcelable